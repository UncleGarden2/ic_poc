# pylint test

import os
from subprocess import Popen, PIPE

path = "../.."
for root, dirs, files in os.walk(path):
    for filename in files:
        if filename.endswith(".py"):
            process = Popen(['pylint', '--reports=n', os.path.join(root, filename)], stdout=PIPE, stderr=PIPE)
            stdout, stderr = process.communicate()
            if stdout:
                print stdout
            if stderr:
                print stderr
